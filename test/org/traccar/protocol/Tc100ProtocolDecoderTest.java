package org.traccar.protocol;

import org.junit.Test;
import org.traccar.ProtocolTest;

public class Tc100ProtocolDecoderTest extends ProtocolTest {

    @Test
    public void testDecode() throws Exception {

        Tc100ProtocolDecoder decoder = new Tc100ProtocolDecoder(new Tc100Protocol());

        verifyPosition(decoder, binary(
                "29298e001b313131313131313131313131313131170321074350032054480355056200610207ffff000002fc00000008780901000034200d"));

        verifyNothing(decoder, binary(
                "2929b1000605162935b80d"));
    }

}
